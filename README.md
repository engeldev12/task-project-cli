# kata TDD

## Task Project

[![pipeline status](https://gitlab.com/engeldev12/task-project-cli/badges/master/pipeline.svg)](https://gitlab.com/engeldev12/task-project-cli/-/commits/master)  [![coverage report](https://gitlab.com/engeldev12/task-project-cli/badges/master/coverage.svg)](https://gitlab.com/engeldev12/task-project-cli/-/commits/master)  

Esta kata tiene el fin de enseñar el proceso de TDD  
construyendo una pequeña aplicación de consola para el  
manejo de tareas.


## Herramientas utilizadas

* **Lenguaje**: Ruby 2.5.1
* **Framework de Test**: test-unit
* **Rake** para el manejo de las tareas

## Lista de requerimientos

* Cada tarea debe tener un nombre.
* Debo poder marcar las tareas como terminadas.
* Al crear una tarea, no estará terminada.
* Da error si intento terminar una tarea que ya lo está.

## Ejecución de las pruebas

* Abre una consola y tipea: 
```ruby

	rake

```
Esto correrá todas las pruebas.

## Tareas automatizadas en rake

* run_test:unit : Corre las pruebas unitarias.