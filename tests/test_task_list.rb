require "test_helper"


class TaskListTest < Test::Unit::TestCase

	def setup
		@tasks = TaskList.new
		@task = Task.new name: "Comer"		
	end	
	
	def test_al_crearla_estara_vacia
		
		assert_empty @tasks.all

	end
	
	def test_al_agregar_una_tarea_no_estara_vacia

		@tasks.add @task
	
		assert_not_empty @tasks.all

	end
	
	def test_contar_tareas_terminadas

		@task.mark_as_done
		
		@tasks.add @task
		@tasks.add Task.new name: "Sin terminar"	
		
		assert_equal 1, @tasks.dones.size
	
	end

	def test_contar_tareas_pendientes
		
		@tasks.add @task

		assert_equal 1, @tasks.undones.size

	end
	
	def test_encotrar_tareas_por_fecha_de_creacion
		
		@tasks.add @task

		@results = @tasks.find_by_created_at Date.today

		assert_equal 1, @results.size

	end

	def test_no_hay_tareas_para_la_fecha_de_creacion_buscada
		
		@results = @tasks.find_by_created_at Date.today

		assert_empty @results

	end

end
