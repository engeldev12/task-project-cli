require "test/unit"
require "simplecov"

$LOAD_PATH << File.expand_path(File.join(__dir__, '..', 'lib'))

SimpleCov.start do 
	add_filter '/tests/'
	add_filter '/vendor/'
end

require 'tasks_project'