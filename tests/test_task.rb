require "test_helper"
require "date"


class TaskTest < Test::Unit::TestCase

	def setup
		@task = Task.new name: "Programar con TDD"
	end	

	def test_al_crearla

		assert_that_not_is_done @task
		assert_that_created_at_is_today @task

	end

	def test_al_marcar_como_terminada_lo_estara

		@task.mark_as_done

		assert @task.is_done
		assert_that_finished_at_is_today @task

	end

	def test_da_error_si_intento_terminar_una_que_ya_lo_esta
		
		@task.mark_as_done

		assert_raise(Task::IsDoneError) { @task.mark_as_done }

	end

	def assert_that_finished_at_is_today(task)
		assert_equal Date.today, task.finished_at
	end

	def assert_that_not_is_done(task)
		refute task.is_done
	end

	def assert_that_created_at_is_today(task)
		assert_equal Date.today, task.created_at
	end 	

end
