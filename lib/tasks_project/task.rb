require "date"


class Task
	
	attr_reader :name, :is_done, :created_at, :finished_at

	IsDoneError = Class.new(Exception)

	def initialize(name:)
		@name = name
		@is_done = false
		@created_at = Date.today
		@finished_at = nil
	end

	def mark_as_done
		
		if @is_done
			raise IsDoneError, 'Ya fue terminada!'
		end
			
		@is_done = true
		@finished_at = Date.today

	end

end
