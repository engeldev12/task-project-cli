class TaskList
		
	def initialize
		@list = []
	end

	def add task
		@list.push task
	end
	
	def all
		@list	
	end

	def dones
		all.select { |task| task.is_done == true }
	end

	def undones
		all.select { |task| task.is_done == false }
	end

	def find_by_created_at date
		all.select { |task| task.created_at == date }
	end	

end