require 'rake/testtask'

namespace :run_test do
		
	desc "Correr pruebas unitarias"
	
	Rake::TestTask.new(:unit) do |t|
		t.libs << 'tests'
		t.verbose = false
		t.test_files = FileList['tests/test_*.rb']
	end

end
